<?php

namespace L2T\Modular\Providers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * @var string
     */
    private $config = 'modular';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->routes(function () {
            foreach (config($this->config. '.modules', []) as $module => $isTurnedOn) {
                if ($isTurnedOn) {
                    $routePrefix = config($module. '.prefix') ?? '/';
                    $modulePath = app_path() . '/Modules/' . Str::studly($module) . DIRECTORY_SEPARATOR;
                    $moduleRoutes = $modulePath . 'routes.php';
                    if (File::exists($moduleRoutes)) {
                        Route::prefix(langPrefix($routePrefix))
                            ->middleware(config($module. '.route_middleware', ['web']))
                            ->namespace('App\\Modules\\' .Str::studly($module). '\\Controllers')
                            ->group($moduleRoutes);
                    }
                }
            }
        });
    }
}
