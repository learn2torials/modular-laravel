<?php

namespace L2T\Modular\Database;
use Illuminate\Database\Seeder as BaseSeeder;

class Seeder extends BaseSeeder
{
    /**
     * @var string
     */
    private $config = 'modular';

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach (config($this->config. '.modules', []) as $module => $isTurnedOn) {
            if ($isTurnedOn && $seeders = config($module. '.seeder', [])) {
                foreach ($seeders as $seeder) {
                    $seed = basename($seeder, ".php");
                    if(class_exists($seed)) {
                       $this->call($seed);
                    }
                }
            }
        }
    }
}
