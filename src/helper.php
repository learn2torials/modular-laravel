<?php

use Illuminate\Support\Str;

/**
 * Created by PhpStorm.
 * User: spatel
 * Date: 06/01/19
 * Time: 9:21 AM
 */
if ( isset($_SERVER["HTTP_CF_CONNECTING_IP"]) )
{
     $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
}

if (!function_exists('langPrefix') )
{
    function langPrefix($prefix='')
    {
        return trim(config('langPrefix'). ($prefix ? '/' .$prefix : ''), '/');
    }
}